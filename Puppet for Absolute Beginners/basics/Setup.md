# Setup Puppet Environment

Primeiro, acesse o ![site do Puppet](https://puppet.com)

Para acessar a documentação acesse https://puppet.com/docs/

Aqui podemos ver os tipos de Puppet, seja Open Source ou Enterprise.

Para nosso curso, vamos usar o Vagrant para bootar as máquinas. As configs estão no arquivo Vagrantfile

Para esse lab, utilizaremos 1vCPU e 1GB de RAM.

E também devemos adicionar os servidores agents no /etc/hosts do master, para persistência de dados

---

## Install Master

Não é possível instalar Puppet Master no Windows.

E o firewall deve estar liberado para a porta 8140. Outra questão imporante é a sincronização de tempo.

Como vamos usar o Centos no Master, siga os seguintes passos para instalação:

```
sudo rpm -ivh https://yum.puppetlabs.com/puppetlabs-release-el-7.noarch.rpm
sudo yum install puppetserver -y
sudo systemctl start puppetserver
sudo systemctl status puppetserver
```

Para instalar o Agent no Ubuntu:

```
sudo apt-get install puppet-agent
```

CentOS/RHEL:

```
sudo yum install puppet
```

Temos um arquivo hosts, que pode ser usado em todos os nós de nossa automação.

Temos também uma arquivo de ntp.conf para guias futuros.

Caso tenha alguma dúvida, eu segui esse tutorial ![aqui](https://www.digitalocean.com/community/tutorials/how-to-install-puppet-4-in-a-master-agent-setup-on-centos-7)

## Install Agents & Ceritificates

### Pré-requisitos:

1. Agent e Master devem estar na mesma network
2. Persistent Hostname = precisa estar no /etc/hosts
3. Time Sync = ntp
4. Firewall para porta 8140

---

### AWS 

Na AWS, não esqueça de liberar a regra de ICMP IPv4 Ping no Security Group

---

## Install

O comando para instalar o agent é

```
sudo yum install puppet-agent
```

Usando o mesmo RPM do Master.

Agora vamos ver os arquivos de configuração

```
ls -lrt /etc/puppetlabs/puppet/puppet.conf
```

Veja esse arquivo no master.

```
[main]
certname = puppetagent01.example.com
server = puppetmaster.example.com
```

E agora crie um link simbólico entre

```
ln -s /opt/puppetlabs/bin/puppet /usr/bin/puppet
```

E agora

```
puppet cert list --all
```

Veja os certificados que falta assinar o certificado entre os nodes.

Agora, falta o master reconhecer o agent.

```
systemctl start puppet
puppet cert list
```

Para assinar um cert

```
puppet cert [url]
```

E o arquivo /etc/puppetlabs/puppet/autosign.conf deve conter uma linha com *.example.com

