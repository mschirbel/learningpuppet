# Understandint Puppet

Imagine um ambiente com 10 RHEL. Suponha que você deseja criar usuários em todos eles.
Uma solução é entrar em cada servidor, e criar o user.
Podemos também criar um script, mas precisamos de conhecimentos de programação e de um Bastion Server

Mas e se houvessem diversos SO?

Para criar o usuário, precisaríamos de conhecimento de cada SO, e após encontrar o comando correto, teríamos um problema com os parâmetros.

Foi a partir dessa necessidade que surgiu o Puppet.

Por exemplo, independente do SO, para criar um usuário com o Puppet:

```puppet
user { "mschirbel":
    ensure => "present",
}

```

Para instalar alguma package:

```
# instalar ntp
package { "ntp":
    ensure => "present",
}

# configurar ntp
file { "/etc/ntp.conf":
    ensure  => "present",
    content => "server 0.centos.pool.ntp.org iburst \n",
}

# start service ntp
service { "ntpd":
    ensure => "running",
}
```

## Idempotency

Puppet consegue enxergar se alguma mudança já está feita, e não solta um erro quando passar por aquela tarefa.