# DSL - Domain Specific Language

Todo recurso tem a mesma cara:

```
<resource> { <TITLE>:
    <Atributo> => <Value>,
}
```

Por exemplo:

```
file { "/etc/ntp.conf":
    ensure => "present",
    content => "server pool.ntp.org\n",
}
```

Podemos não colocar a "," para o último atributo.

E é recomendado colocar aspas em cada value.

Por exemplo:

```
# instalar ntp
package { "ntp":
    ensure => "present",
}

# configurar ntp
file { "/etc/ntp.conf":
    ensure  => "present",
    content => "server 0.centos.pool.ntp.org iburst \n",
}

# start service ntp
service { "ntpd":
    ensure => "running",
}
```

Para descubrir algum resource, tente assim:

```
puppet resource --types |grep -i [resource]
```

Depois podemos ver as informações detalhadas:

```
puppet describe [resource]
```

Para aplicar uma mudança:

```
puppet apply
```

---

Temos 49 recursos habilitados por default no Puppet. Podemos rodar:

```
puppet help resource --types | more
```

O output será os tipos que teremos para o puppet resources.

E para encontrar os atributos de um resource:

```
puppet help resource --types | grep -i user
puppet describe user
```

---

## Developing

Para isso, crie uma pasta para salvar os códigos e crie um arquivo .pp

```
mkdir /var/tmp/demo
cd /var/tmp/demo
vi user.pp
```

Dentro do arquivo:

```
user { "mschirbel":
    ensure => "present",
}
```

---

Para verificar se o código está OK:

```
puppet parser validate user.pp
```

Para fazer um test:

```
puppet apply user.pp --noop
```

Para aplicar o código:

```
puppet apply user.pp
```

Se você rodar de novo, nada vai acontecer, pois o código é indepotente.

---

Vamos criar um usuário:

```
user { "mschirbel":
    ensure => "present",
    uid    => "7777",
    shell  => "/bin/sh",
}
```

Valide, teste e aplique o código.

---

Crie um novo arquivo filedemo.pp e insira o seguinte código:

```
file { "/var/tmp/testfile":
    ensure => "present",
    owner  => "root",
    group  => "root",
    mode   => "0777",
}
```

Valide, teste e aplique o código.

---

Agora vamos tentar instalar uma package:

Crie um novo arquivo packagedemo.pp:

```
package { "telnet":
    ensure => "present",
}
```

Valide, teste e aplique o código.

---

Agora vamos tratar alguns serviços:

Crie um novo arquivo servicesdemo.pp:

```
service { "nfs":
    ensure => "running",
}
```

Valide, teste e aplique o código.

---

Agora vamos criar um arquivo com multiplos resources:

```
file { "/var/tmp/testdir":
    ensure => "directory",
}

file { "/var/tmp/testdir/testfile":
    ensure  => "present",
    owner   => "root",
    group   => "root",
    mode    => "0777",
    content => "Test file for Puppet!\n",
}
```

Valide, teste e aplique o código.