# Classes

São grupos que juntam recursos comuns.

Até agora, só criamos arquivo .pp e executando.

Mas e se quiséssemos subir um 3-tier-web app?

Classes servem para reaproveitar o código e para dividir o código em pequenas partes para cada uma das partes envolvidas.

Para criar uma classe:

```
class <class name> {
    <resources>
}
```

Exemplo:

```
class ntpconfig {
    package { "ntp":
        ensure => "present",
    }

# configurar ntp
    file { "/etc/ntp.conf":
        ensure  => "present",
        content => "server 0.centos.pool.ntp.org iburst \n",
    }

# start service ntp
    service { "ntpd":
        ensure => "running",
    }
}

include ntpconfig
```

Para usar uma classe, devemos incluir em algum lugar. É o que estamos falando no include. Nesse caso, estamos incluindo a classe na própria declaração.

Os nomes das classes devem ser únicos, assim como os recursos.

Valide, teste e aplique o código.