# Architecture

O agents manda os dados para o master usando FACTS.

Facter é uma assistente de invetário. Facts tem informações do sistema.

Essas informações podem ser usadas como variáveis no código do Puppet

O Master cria o Catalog e compara o que recebeu do agent com o Catalog.

Catalog é a diferença entre o que é esperado e o que existe.

Envia para o agent o Catalog.

Após receber, o agent começa a executar as mudanças. Durante o processo, o agent envia Reports para o Master.

## Building Blocks

Resources - são funções internas que funcionam no backend que fazem as mudanças acontecer.

Class - grupo de recursos ou operações. Como um time, que juntos alcançam um objetivo.

Manifest - São diretórios contendo arquivos .pp(Puppet Program)

Modules - são coleções de manifestos ou classes. Por exemplo, MySQL Module