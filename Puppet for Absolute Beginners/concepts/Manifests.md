# Manifests

Puppet tem um diretório bem definido para evitar ficar criando códigos em lugares diferentes.

Puppet é desenhado de uma forma que todos os arquivos devem ficar dentro de diretórios Manifestos.

O path default pode ser encontrado:

```
puppet config print
```

Sempre que o agent é conectado ao master, o master olha o catalog dentro dos manifests, para ver se há alguma alteração.

## Site.pp

É o principal entrypoint da network do Puppet. Também conhecido como Site Manifest.

Esse aquivo está em

*/etc/puppetlabs/code/environments/production/manifests/site.pp*

Lá declararemos todas as classes.

---

## Deploy com Manifest

Para encontrar todos os agents associados a um master:

```
puppet cert list --all
```

Se você estiver usando a versão grátis do Puppet, deverá criar o arquivo site.pp dentro de */etc/puppetlabs/code/environments/production/manifests/* dentro do Master.

No agent:

```
puppet agent -tv
```

Para ver se o agent consegue se conectar com o Master

Coloque o seguinte código dentro do site.pp:

```
class ntpconfig {
    package { "ntp":
        ensure => "present",
    }

# configurar ntp
    file { "/etc/ntp.conf":
        ensure  => "present",
        content => "server 0.centos.pool.ntp.org iburst \n",
    }

# start service ntp
    service { "ntpd":
        ensure => "running",
    }
}

include ntpconfig
```

Valide o código.

Se você rodar um 

```
puppet agent -tv
```

O agent vai comparar os catalogs e aplicar as mudanças.

Valide se o ntpd foi instalado:

```
systemctl status ntpd
```