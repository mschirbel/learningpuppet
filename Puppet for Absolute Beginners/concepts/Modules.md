# Modules

Puppet tem um diretório default onde podemos desenvolver nossos módulos.

```
puppet config print |grep -i module
```

Assim você encontrará o diretório default do puppet para os módulos.

Para fazer qualquer ação de módulos:

```
puppet modules
```

Para criar um novo modulo:

```
puppet module generate <author>-<module-name>
```

Depois disso, puppet pergunta várias coisas, podemos ignorar todas.

Com isso, teremos um novo diretório, com diversos arquivos.

Dentro do diretório *manifests/* é onde escreveremos nossos códigos.

Dentro desse diretório temos o arquivo init.pp, que é onde iniciaremos nossas classes. Uma delas sendo o nome do modulo.

Podemos validar o arquivo:

```
puppet parser validate init.pp
```

Sabemos que o site.pp é o primeiro arquivo que o agent vai procurar, então precisamos declarar nossa classe dentro desse arquivo.