# Resources

1. File
2. User
3. Computer
4. Router
5. Package
6. etc

São funções já estabelecidas para serem usadas em Cloud, VM ou Bare Metal.

## Tipos de Resources

Existem os Core Resources, são aqueles que já vem no Puppet, assim que feito o download. São mantidos pelo time oficial do Puppet.

Existem os Defined Resources eles tem a flexibilidade escritos na linguagem declarativa do Puppet.

Existem os Custom Resources, escritos em Ruby.

Para saber os argumentos necessários, podemos usar "--help".