# Node Definitions

Node é como restringimos o código a algumas máquinas.

Todos os agents vao olhar o Manifest do Master. Mas se quisermos que apenas algum olhe, temos que usar os Nodes.

Para isso, fazemos da seguinte forma:

```
node "hostname" {
    include <class name>
}
```

Podemos incluir múltiplos hostnames, basta colocar "hostname1", "hostname2".

Nunca usamos o site.pp para escrever código. Nele somente colocamos node definitions para as classes.

Os códigos ficam dentro de *manifests/*

Caso algum dos agents não esteja definido nos nodes, vai dar um erro 500.