# Curso de Puppet para Iniciantes

Professor: Yogesh Raheja - yogeshraheja07@gmail.com

## Puppet

É uma ferramenta open-source de automação.

Puppet é baseado em Ruby. Foi feito para fazer a instalação, configuração e deploy em máquinas remotas.

### Deployment Model

Existem 2 tipos:

1. Standalone - dev/homol
2. Master-Agent - prod

## OS 

Puppet suporta vários SO: RHEL, Centos, Ubuntu, Windows, MAC, Solaris e etc.

---

Puppet tem um estilo de programação declarativa. Ou seja, apenas apresentamos o que queremos, o Puppet vai decidir como fazer o que **declaramos**.

Puppet é escalável, sendo possível automatizar uma grande quantidade de nodes.

---

Temos dois tipos de máquinas na arquitetura:

O master, aquele que detém o código
O agent, aquele que vai sofrer a atualização. E checa com o master se há updates a cada 1800 secs.

O agent vai solicitar updates, temos um Pull de Configurations.

## Push vs Pull

Em um modelo de deploy baseado em push o master manda as configurações para os agents. Sempre iniciada pelo master. Como Ansible, por exemplo.

Em um modelo de deploy baseado em pull, o agent se conecta com o master e vê quais as diferenças em configurações. Ou seja, se inicia pelo agent. Como Puppet e Chef, por exemplo.

## Execution Flow

Puppet master é um node com software instalado e configurado. Contém diferentes configs do ambiente. E serve para mudar os agents, somente trocando em um único lugar.

A comunicação é feita por meio de certificados, baseados na porta default 8140;

Uma vez que a comunicação é feita:

1. Facts = o agent manda a config atual dele para o master
2. Catalog = comparação entre a config do agent com a que foi alterada no master
3. Report = após receber o Catalog, o agent faz as mudanças e envia um report para o master